Files used for FTBF pixel project simulation using [Allpix Squared](https://gitlab.cern.ch/allpix-squared/allpix-squared)\
Check Allpix Squared package user manuel [here](https://project-allpix-squared.web.cern.ch/project-allpix-squared/usermanual/allpix-manual.html) online or donwload pdf version [here](https://project-allpix-squared.web.cern.ch/project-allpix-squared/usermanual/allpix-manual.pdf)

Check useful tutorial [here](https://indico.cern.ch/event/738283/sessions/290645/attachments/1759057/2853912/dhynds-AP2workshop2018-tutorial.pdf)


Package info
==================
1.config file
-------------------
The framework has the following three required layers of configuratin files(can also set visualization in config files):
- The main configuration, which is passed directly to the binary. Contains both the global framework configuration and the list of modules to instantiate together with their configuration. An example can be found [here](https://gitlab.cern.ch/allpix-squared/allpix-squared/blob/master/examples/example.conf)
- The detector configuration passed to framework to determine the geometery. Describes the detector setup, containing the position, orientation and model type of all detectors. An example can be found [here](https://gitlab.cern.ch/allpix-squared/allpix-squared/blob/master/examples/example_detector.conf)
- The detector model configuration. Contains the parameters describing a particular type of detector. Several models are already provided by the [framework](https://gitlab.cern.ch/allpix-squared/allpix-squared/tree/master/models). See [models/test.conf](https://gitlab.cern.ch/allpix-squared/allpix-squared/blob/master/models/test.conf) in the repository for an example. Refer to [Section 9.3](https://project-allpix-squared.web.cern.ch/project-allpix-squared/usermanual/allpix-manualch9.html#x10-1900009.3) for more details.

2.units
--------------------------
Check details in [Chapter 7](https://project-allpix-squared.web.cern.ch/project-allpix-squared/usermanual/allpix-manualch7.html#x8-640007)\
For an arithmetic type, it may have a suffix indicating the unit. The list of base unit is shown in [this table](https://project-allpix-squared.web.cern.ch/project-allpix-squared/usermanual/allpix-manualch4.html#x5-16001r1)\
Please use consistent unit in your config.

Run simulations 
===================
1.allpix is not installed -- Docker images
--------------------------------
Docker images are provided for the framework to allow anyone to run simulations without the need of installing Allpix2 on their system. The only requried program is the Docker executable, all other dependencies are provided within the Docker images. In orfer to exchane configuration files and output data between the host system and the Docker container, a folder from the host system should be mounted to the container's data path /data, which also acts as the Docker WORKDIR location.

The following command creates a container from the latest Docker image in the project registry and start an interative shell session with the allpix executable already in the $PATH. Here, the current host system path is mounted to the /data directory of the container.

docker run --interactive --tty --volume "$(pwd)":/data --name=allpix-squared gitlab-registry.cern.ch/allpix-squared/allpix-squared bash

Alternatively it is also possible to directly start the simulation instead of an interactive shell, e.g. using the following command:\
docker run --tty --rm --volume "$(pwd)":/data --name=allpix-squared gitlab-registry.cern.ch/allpix-squared/allpix-squared "allpix -c my\_simulation.conf"

2.allpix is installed
-------------------------
instruction to run simulation: ./../bin/allpix -c your\_main\_config.conf\
Allpix options:\
-c `<file>`    configuration file to be used\
-l `<file>`    file to log to besides standard output\
-o `<option>`  extra module configuration option(s) to pass\
-g `<option>`  extra detector configuration options(s) to pass\
-v `<level>`   verbosity level, overwriting the global level\
--version    print version information and quit


Config file details
========================
[DepositionGeant4]\
physics\_list check info [here](https://geant4.web.cern.ch/node/1731) \
particle\_type check info [here](http://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/ForApplicationDeveloper/html/TrackingAndPhysics/particle.html)\
other source related parameters -- do I have to filled them?

[CapacitiveTransfer]


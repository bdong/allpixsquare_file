################
1. FourPlanes: first four planes settings from test beam setting\
   reconstruction path: /afs/cern.ch/work/b/bdong/proteus/proteus/Simulation_study/FourPlanes
2. FivePlanes: first five planes settings from test beam setting\
   reconstruction path: /afs/cern.ch/work/b/bdong/proteus/proteus/Simulation_study/FivePlanes
3. SevenPlanes: six planes setting from test beam setting and added a no rorate plane between the 3rd and 4th planes\
   reconstructoin path: /afs/cern.ch/work/b/bdong/proteus/proteus/Simulation_study/SevenPlanes
4. SevenPlanes_nororation: seven planes with same setting, no roration.\
   reconstruction path: /afs/cern.ch/work/b/bdong/proteus/proteus/Simulation_study/SevenPlanes_norotation
5. SevenPlanes_rotated: six planes setting from test beam setting except no twisted angel on 2nd and 4th telescope\
   reconstruction path: /afs/cern.ch/work/b/bdong/proteus/proteus/Simulation_study/SevenPlanes_rotated
6. the ones with numbers, means changed one side of the pixed width
